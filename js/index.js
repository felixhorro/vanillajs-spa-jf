(function (people) {
	if (!people) return;

	const logContainer = document.querySelector("div.log");
	function log(text, color) {
		const log = document.createElement("p");
		log.style.backgroundColor = color;

		const time = document.createElement("span");
		time.classList.add("timer");
		time.textContent = new Date().toLocaleTimeString();

		//const textNode = document.createTextNode(text);

		log.append(time, text);

		logContainer.append(log);
	}

	const backgroundColors = [
		"red",
		"",
		"lightseagreen",
		"blue",
		"",
		"yellow",
		"",
		"",
		"",
		"",
		"",
		"",
		"",
		"lightskyblue",
		""
	]

	const getRandomPerson = () => people[Math.floor(Math.random() * people.length)];
	for (let i = 0; i < 10; i++) {
		setTimeout(() => {
			const person = getRandomPerson();
			log(
				`${person.name}, de ${person.homeworld}, saluda!`,
				backgroundColors[i]
			);
		}, i * 400);
	}

})(window.swapi && window.swapi.people);
